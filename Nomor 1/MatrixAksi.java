package com.tugas;

public class MatrixAksi {

    public static void main(String[] args) {
        double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
        Matrix D = new Matrix(d);
        double[][] a = { { 2, 3, 5}, {9, 6, 3}, {5, 3, 3}};
        double[][] c = { { 1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        Matrix A = new Matrix(a);
        Matrix B = A.transpose();
        Matrix C = new Matrix(c);

        //Matriks hasil penjumlahan A dan B
        double[][] p = { { 4, 12, 10}, {8, 9, 6}, {14, 9, 6}};
        Matrix P = new Matrix(p);

        System.out.println("\nMatrik D");
        D.show();

        System.out.println("\nMatrik A");
        A.swap(1, 2);
        A.show();
        System.out.println();

        // shouldn't be equal since AB != BA in general
        System.out.println(A.times(B).eq(B.times(A)));
        System.out.println();
        Matrix b = Matrix.random(3, 1);
        b.show();

        System.out.println("\nMatrik B (Transpose Matriks A)");
        B.show();

        System.out.println("\nMatrik C (Matriks Indentitas)");
        C.show();

        System.out.println("\nMatrik P (Matriks A+B)");
        P.show();

        System.out.println();
        Matrix x = A.solve(b);
        x.show();

        System.out.println();
        A.times(x).show();
    }
}
